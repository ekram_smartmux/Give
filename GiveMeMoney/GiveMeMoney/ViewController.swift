//
//  ViewController.swift
//  GiveMeMoney
//
//  Created by SmartMux-Rabbi on 3/26/18.
//  Copyright © 2018 SmartMux Limited. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var monyLabel: UILabel!
    
    //Global Variable 
    
    var money:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionConsumable(sender: AnyObject) {
    
        print("Consumable")
    }

    @IBAction func actionNonConsumable(sender: AnyObject) {
        print("NonConsumable")
    }
    @IBAction func actionAutoRenewable(sender: AnyObject) {
        print("Auto Renewable")
    }


    @IBAction func actionNonRenewable(sender: AnyObject) {
        print("NonRenewable")
    }

}

